{!!Form::label('title','Title: ')!!}<br/>
{!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter a title']) !!}<br/>
{!!Form::label('text','Text: ')!!}<br/>
{!!Form::textarea('text',null,['class'=>'form-control', 'placeholder'=>'Enter a text','rows' => 10,'required','min'=>20]) !!}<br>
{!!Form::file('picturepath',['enctype'=>'multipart/form-data'])!!}<br>
