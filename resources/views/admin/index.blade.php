@extends('layouts.principal-admin')
@section('content')
<div class="col-md-12">
  <div class="panel panel-filled">
    <div class="panel-heading">
      <h1 class="page-header">All news</h1>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            This is all news stored in data base
          </div>
          <div class="col-md-6">
            <a href="{!! URL::to('admin/create') !!}" class="btn btn-w-md btn-accent pull-right">Add new record <i class="fa fa-plus"></i></a>
          </div>
        </div>
      </div><br/>
      @include('alerts.success')
      @include('alerts.error')
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th class="text-center">Id</th>
              <th class="text-center">Tittle</th>
              <th class="text-center">Text</th>
              <th class="text-center">Picture</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          @foreach($news as $new)
          <tbody>
            <tr>
              <td>{{ $new->id }}</td>
              <td>{{str_limit($new->title,$limit = 45,$end='...')}}</td>
              <td>{{str_limit($new->text,$limit = 45,$end='...')}}</td>
              @if($new->picturepath == null)
              <td><img src="images/no-image.jpg" alt="" style="width:100px"/></td>
              <td><div class="form-group" align="center"><a href="{!! route('admin.show',$parameters = $new->id) !!}" class="btn btn-success btn-circle" data-tooltip="Ver detalle"><i class="fa fa-search"></i></a>
              <a href="{!! route('admin.edit',$parameters = $new->id) !!}" class="btn btn-primary btn-circle" data-tooltip="Editar new"><i class="glyphicon glyphicon-edit"></i></a>
              <a href="{!! route('admin.destroy',$parameters = $new->id) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar new" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="glyphicon glyphicon-trash"></i></a></div>
            </td>
            @else
            <td><img src="images/{{$new->picturepath}}" alt="" style="width:100px"/></td>
            <td>
              <div class="form-group" align="center"><a href="{!! route('admin.show',$parameters = $new->id) !!}" class="btn btn-success btn-circle" data-tooltip="Ver detalle"><i class="fa fa-search"></i></a>
              <a href="{!! route('admin.edit',$parameters = $new->id) !!}" class="btn btn-primary btn-circle" data-tooltip="Edit new"><i class="glyphicon glyphicon-edit"></i></a>
              <a href="{!! route('admin.destroy',$parameters = $new->id) !!}" class="btn btn-danger btn-circle" data-tooltip="Eliminar new" onclick='return confirm("¿Estas seguro de querer eliminar?")'><i class="glyphicon glyphicon-trash"></i></a></div>
            </td>
            @endif
          </tr>
        </tbody>
        @endforeach
      </table>
      {!!$news->render()!!}
    </div>
  </div>
</div>
</div>
@endsection