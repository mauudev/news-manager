@extends('layouts.principal-admin')
@section('content')
{!!Form::model($new,['route'=>['admin.update',$new->id],'method'=>'PUT','files' => true])!!}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <h1 class="page-header">Update new data</h1>
                Enter information below
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        {!!Form::hidden('administrators_id', Auth::user()->id)!!}
                        {!!Form::label('title','Title: ')!!}
                        {!!Form::text('title',null,['id'=>'newsTitle','class'=>'form-control','required','min'=>10]) !!}<br>
                        {!!Form::label('text','Text: ')!!}
                        {!!Form::textarea('text',null,['id'=>'newsText','class'=>'form-control', 'rows' => 10,'min'=>20,'required']) !!}<br>
                        {!!Form::label('picturepath','Image:')!!}
                        {!!Form::file('picturepath',['class'=>'filestyle', 'data-input'=>'false','enctype'=>'multipart/form-data'])!!}<br>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5">
                        {!!Form::label('image','Image: ')!!}<br/>
                        @if($new->picturepath == null)
                            <img src="../../images/no-image.jpg" border="4" style="width:240px" />
                        @else
                            <img src="../../images/{{ $new->picturepath }}" style="width:240px" />
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
       {!!Form::submit('Update',['class'=>'btn btn-w-md btn-primary'])!!} <a class="btn btn-w-md btn-danger" href="{!! URL::to('admin') !!}">Cancel</a>
    </div>
</div>
{!! Form::close() !!}
@endsection