@extends('layouts.principal-admin')
@section('content')
<div class="col-md-12">
    <div class="panel panel-filled">
        <div class="panel-heading">
            <h1 class="page-header">Details</h1>
            Details of new data
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><h4><b>Title: </b></h4></td>
                            <td><h5>{{ $new->title }}</h5></td>
                        </tr>
                        <tr>
                            <td><h4><b>Text: </b></h4></td>
                            <td><h5>{{ $new->text }}</h5></td>
                        </tr>
                        <tr>
                            <td><h4><b>Created at: </b></h4></td>
                            <td><h5>{{ $new->created_at }}</h5></td>
                        </tr>
                        <tr>
                            <td><h4><b>Modified at: </b></h4></td>
                            <td><h5>{{ $new->updated_at }}</h5></td>
                        </tr>
                        <tr>
                            <td><h4><b>Imagen: </b></h4></td>
                            <td>@if($new->picturepath == null)
                                <img src="../images/no-image.jpg" border="4" style="width:240px" />
                                @else
                                <img src="../images/{{ $new->picturepath }}" style="width:240px" />
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <h1 class="page-header"></h1>
                    <a href="{!! URL::to('admin') !!}" class="btn btn-primary">Go back<i class="fa fa-back"></i></a><br><br><br><br>
            </div>
            
        </div>
    </div>
    @endsection