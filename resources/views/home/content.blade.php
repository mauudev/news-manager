@extends('layouts.principal-home')
@section('content')
<br/>
<div class="row row-story">
<div class="col-lg-4">
<div class="contendor-story">
<p id="text1"><em>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em></p>
<div class="contendor-title center-block">
  <h4 id="title1" class="margin"><strong>Title Goes Here!!</strong></h4>
</div>
</div>
</div>
<div class="col-lg-4">
<div class="contendor-story">
<p id="text2"><em>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em></p>
<div class="contendor-title center-block">
  <h4 id="title2" class="margin"><strong>Title Goes Here!!</strong></h4>
</div>
</div>
</div>
<div class="col-lg-4">
<div class="contendor-story center-block">
<p id="text3"><em>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em></p>
<div class="contendor-title center-block">
  <h4 id="title3" class="margin"><strong>Title Goes Here!!</strong></h4>
</div>
</div>
</div>
</div>
@endsection