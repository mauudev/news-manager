@extends('layouts.principal-admin')
@section('content')
<div class="col-md-12">
  <div class="panel panel-filled">
    <div class="panel-heading">
      <h1 class="page-header">All news</h1>
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            This is all news stored in data base
          </div>
          <div class="col-md-6">
            <a href="{!! URL::to('news/create') !!}" class="btn btn-w-md btn-accent pull-right">Add new record <i class="fa fa-plus"></i></a>
          </div>
        </div>
      </div><br/>
      @include('alerts.success')
      @include('alerts.error')
      @include('alerts.success-ajax')
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th class="text-center">Id</th>
              <th class="text-center">Tittle</th>
              <th class="text-center">Text</th>
              <th class="text-center">Picture</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
        <tbody id="dataTable1"></tbody>
      </table>
      <div class="m-t-md">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header text-center">
                <h4 id="headTittle" class="modal-title"></h4>
                <small>News tittle</small>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <input type="hidden" id="id">
                    <!-- content -->
                    {!!Form::hidden('administrators_id', Auth::user()->id)!!}
                    {!!Form::label('title','Title: ')!!}
                    {!!Form::text('title',null,['id'=>'newsTitle','class'=>'form-control','required','min'=>10]) !!}<br>
                    {!!Form::label('text','Text: ')!!}
                    {!!Form::textarea('text',null,['id'=>'newsText','class'=>'form-control', 'rows' => 10,'min'=>20,'required']) !!}<br>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="update" type="button" class="btn btn-default">Update</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
{!! Html::script('js/read-news-ajax.js') !!}
@endsection


