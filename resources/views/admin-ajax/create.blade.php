@extends('layouts.principal-admin')
@section('content')
{!!Form::open(['route'=>'news.store', 'method'=>'POST','files' => true])!!}
{!!Form::hidden('administrators_id', Auth::user()->id)!!}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <h1 class="page-header">Register new news data</h1>
                Enter information below
            </div>
            <div class="panel-body">
               @include('admin.form.form')
            </div>
        </div>
    </div>
    <div class="col-md-6">
       {!!Form::submit('Register',['class'=>'btn btn-w-md btn-primary'])!!} <a class="btn btn-w-md btn-danger" href="{!! URL::to('news') !!}">Cancel</a>
    </div>
</div>
{!! Form::close() !!}
@endsection