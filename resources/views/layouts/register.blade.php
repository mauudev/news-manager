<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/luna_admin-v1.1/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:24 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>

    <!-- Page title -->
    <title>News manager | Register</title>

    <!-- Vendor styles -->
        {!! Html::style('vendor/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('vendor/animate.css/animate.css') !!}
        {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}.
        {!! Html::style('vendor/toastr/toastr.min.css') !!}
        <!-- App styles -->
        {!! Html::style('styles/pe-icons/pe-icon-7-stroke.css') !!}
        {!! Html::style('styles/pe-icons/helper.css') !!}
        {!! Html::style('styles/stroke-icons/style.css') !!}
        {!! Html::style('styles/style.css') !!}
</head>
<body class="blank">

<!-- Wrapper-->
<div class="wrapper">


    <!-- Main content-->
    <section class="content">
        <div class="container-center lg animated slideInDown">
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-add-user"></i>
                </div>
                <div class="header-title">
                    <h3>Register</h3>
                    <small>
                        Please enter your data to register.
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                    <p>
                    </p>
                    {!!Form::open(['route'=>'register.store', 'method'=>'POST'])!!}
                        <div class="row">
                            <div class="form-group col-lg-6">
                                {!! Form::label('name','Name: ') !!}
                                {!! Form::text('name',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
                                <span class="help-block small">Your unique username to app</span>
                            </div>
                            <div class="form-group col-lg-6">
                                {!! Form::label('last_name','Last name: ') !!}
                                {!! Form::text('last_name',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
                                <span class="help-block small">Your unique username to app</span>
                            </div>
                            <div class="form-group col-lg-6">
                                {!! Form::label('email','E-mail: ') !!}
                                {!! Form::text('email',null,['class'=>'form-control', 'required','minlength'=>5]) !!}<br/>
                                <span class="help-block small">Your address email to contact</span>
                            </div>
                            <div class="form-group col-lg-6">
                                {!! Form::label('password','Password: ') !!}
                                {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Password']) !!}<br/>
                                <span class="help-block small">Your hard to guess password</span>
                            </div>                      
                        </div>
                        <div>
                            {!!Form::submit('Register',['class'=>'btn btn-w-md btn-accent'])!!}
                            <a class="btn btn-w-md btn-success" href="{!! URL::to('login') !!}">Login</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
            {!! Html::script('vendor/pacejs/pace.min.js') !!}
            {!! Html::script('vendor/jquery/dist/jquery.min.js') !!}
            {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
            {!! Html::script('vendor/toastr/toastr.min.js') !!}
            {!! Html::script('vendor/sparkline/index.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.min.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.resize.min.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.spline.js') !!}
            <!-- App scripts -->
            {!! Html::script('scripts/luna.js') !!}
            {!! Html::script('js/script-1.js') !!}
            {!! Html::script('js/script-2.js') !!}

</body>


<!-- Mirrored from webapplayers.com/luna_admin-v1.1/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:24 GMT -->
</html>