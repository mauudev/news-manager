<!DOCTYPE html>
<html>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:16 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Page title -->
        <title>News manager | Login</title>
        <!-- Vendor styles -->
        {!! Html::style('vendor/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('vendor/animate.css/animate.css') !!}
        {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}.
        {!! Html::style('vendor/toastr/toastr.min.css') !!}
        <!-- App styles -->
        {!! Html::style('styles/pe-icons/pe-icon-7-stroke.css') !!}
        {!! Html::style('styles/pe-icons/helper.css') !!}
        {!! Html::style('styles/stroke-icons/style.css') !!}
        {!! Html::style('styles/style.css') !!}
    </head>
    <body class="blank">
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Main content-->
            <section class="content">
                <div class="container-center animated slideInDown">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-unlock"></i>
                        </div>
                        <div class="header-title">
                            <h3>Restricted area</h3>
                            <small>
                            Please enter your credentials to login.
                            </small>
                        </div>
                    </div>
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            {!! Form::open(['route'=>'login.store','method'=>'POST']) !!}
                            <fieldset>
                                <div class="form-group">
                                    <label class="control-label" for="username">Username</label>
                                    {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'E-mail', 'required','autofocus']) !!}
                                    <span class="help-block small">Your unique email to app</span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    {!! Form::password('password', ['class' => 'form-control','placeholder'=>'Password']) !!}
                                    <span class="help-block small">Your strong password</span>
                                </div>
                                <div>
                                    {!! Form::submit('Login',['class'=>'btn btn-w-md btn-accent']) !!}
                                    <a class="btn btn-w-md btn-info" href="{!! URL::to('register') !!}">Register</a>
                                </div>
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @include('alerts.success')
                    @include('alerts.error')
                    @include('alerts.required')
                </div>
            </section>
            <!-- End main content-->
            <!-- End wrapper-->
            <!-- Vendor scripts -->
            {!! Html::script('vendor/pacejs/pace.min.js') !!}
            {!! Html::script('vendor/jquery/dist/jquery.min.js') !!}
            {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
            {!! Html::script('vendor/toastr/toastr.min.js') !!}
            {!! Html::script('vendor/sparkline/index.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.min.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.resize.min.js') !!}
            {!! Html::script('vendor/flot/jquery.flot.spline.js') !!}
            <!-- App scripts -->
            {!! Html::script('scripts/luna.js') !!}
            {!! Html::script('js/script-1.js') !!}
            {!! Html::script('js/script-2.js') !!}
        </body>
        <!-- Mirrored from webapplayers.com/luna_admin-v1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:16 GMT -->
    </html>