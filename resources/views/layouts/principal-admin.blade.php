<html>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:36:21 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Page title -->
        <title>News Manager | Main</title>
        <!-- Vendor styles -->
        {!! Html::style('vendor/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('vendor/animate.css/animate.css') !!}
        {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}.
        {!! Html::style('vendor/toastr/toastr.min.css') !!}
        <!-- App styles -->
        {!! Html::style('styles/pe-icons/pe-icon-7-stroke.css') !!}
        {!! Html::style('styles/pe-icons/helper.css') !!}
        {!! Html::style('styles/stroke-icons/style.css') !!}
        {!! Html::style('styles/style.css') !!}
        
        
        {!! Html::script('vendor/pacejs/pace.min.js') !!}
        {!! Html::script('vendor/jquery/dist/jquery.min.js') !!}
        {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('vendor/toastr/toastr.min.js') !!}
        {!! Html::script('vendor/sparkline/index.js') !!}
        <!-- App scripts -->
        {!! Html::script('scripts/luna.js') !!}
    </head>
    <body>
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Header-->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div id="mobile-menu">
                            <div class="left-nav-toggle">
                                <a href="#">
                                    <i class="stroke-hamburgermenu"></i>
                                </a>
                            </div>
                        </div>
                        <a class="navbar-brand" href="{!! URL::to('admin') !!}">
                            Admin
                            <span>v1.0</span>
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                    @if (Auth::check())
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{Auth::user()->name}}
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                @else               
                 <li class=" profil-link">
                    <a href="{!! URL::to('login') !!}">
                        <span class="profile-address">Login</span>
                        <img src="images/user.png" class="img-circle" alt="">
                    </a>
                 </li>
                @endif
                </ul>   
                </div>
            </nav>
            <!-- End header-->
            <!-- Navigation-->
            <aside class="navigation">
                <nav>
                    <ul class="nav luna-nav">
                        <li class="nav-category">
                            <b>Main</b>
                        </li>
                        <li>
                            <a href="#monitoring" data-toggle="collapse" aria-expanded="false">
                                News<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring" class="nav nav-second collapse">
                                <li><a href="{!! URL::to('admin/create') !!}"> Create new</a></li>
                                <li><a href="{!! URL::to('admin') !!}"> News</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#monitoring1" data-toggle="collapse" aria-expanded="false">
                                News (Ajax version)<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring1" class="nav nav-second collapse">
                                <li><a href="{!! URL::to('news/create') !!}"> Create new</a></li>
                                <li><a href="{!! URL::to('news') !!}"> News</a></li>
                            </ul>
                        </li>
                        <li class="nav-info">
                            <i class="pe pe-7s-shield text-accent"></i>
                            <div class="m-t-xs">
                                <span class="c-white">News manager</span> Administration panel for News Manager application.
                            </div>
                        </li>
                    </ul>
                </nav>
            </aside>
            <!-- End navigation-->
            <!-- Main content-->
            <section class="content">
                @yield('content')
            </section>
            <!-- End main content-->
        </div>
        <!-- End wrapper-->
        <!-- Vendor scripts -->


