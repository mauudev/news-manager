<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Theme Made By www.w3schools.com - No Copyright -->
    <title>Bootstrap Theme Simply Me</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Html::style('bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('bootstrap/css/custom.css') !!}
    <link href="http://www.fontsquirrel.com/fonts/lato;" rel="stylesheet">
    {!! Html::script('bootstrap/js/jquery.min.js') !!}
    {!! Html::script('bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('js/push-new.js') !!}
    <!-- Para el Carousel -->
    {!! Html::style('css/lightslider.css') !!}
    <style>
    ul {
    list-style: none outside none;
    padding-left: 0;
    margin: 0;
    }
    .demo .item {
    margin-bottom: 60px;
    }
    .content-slider li {
    background-color: #ed3020;
    text-align: center;
    color: #FFF;
    }
    .content-slider h3 {
    margin: 0;
    padding: 70px 0;
    }
    .demo {
    width: 800px;
    }
    </style>
  </head>
  <body>
    <!-- Navbar -->
    <!-- Navbar -->
    <nav class="navbar navbar-default nav-custom">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <div class="custom-logo">
            <a class="logo1" href="#">Your</a>
            <a class="logo2" href="#">LOGO</a>
          </div>
        </div>
        <div class="collapse navbar-collapse nav-custom-interno" id="myNavbar">
          <ul class="nav navbar-nav navbar-right menu-custom">
            <li><a href="#">HOME</a></li>
            <li><a href="#">PAGES</a></li>
            <li><a href="#">FEATURE</a></li>
            <li><a href="{!! URL::to('admin') !!}">ADMIN</a></li>
            <li class="last-li"><a href="#">CONTACT</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="imagen-1">
      <img src="img/hero2.jpg" class="imagen-1">
    </div>
    <div class="content">
      <h3 class="h3-triangulo">Full width responsive triangle divider</h3>
      <p>Quis ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
    </div <div class="wrapper">
    <div class="triangle-down">
      <div></div>
    </div>
  </div>
</div>
<div class="container-fluid text-center">
  <div class="content2">
    <div class="col-sm-12">
      <div class="row row-logo ">
        <div class="col-sm-4 ">
          <img src="img/foco.jpg" class="logos" alt="Image">
          <h3 class="margin"><strong>Ideas</strong></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="col-sm-4">
          <img src="img/mundo.jpg" class="logos" alt="Image">
          <h3 class="margin"><strong>Make research</strong></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="col-sm-4">
          <img src="img/engranaje.jpg" class="logos" alt="Image">
          <h3 class="margin"><strong>Do the work</strong></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit , consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur adipisicing elit</p>
        </div>
      </div>
    </div>
  </div> 
  <div class="triangle-down2">
    <div class="fondo2">
      <div></div>
    </div>
  </div>

</div>
</div>
<!-- Carrusel -->
<div class="container-fluid text-center">
<div class="content3">
<h3 class="h3-triangulo">Some Of Our Work</h3>
<p>Quis ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
<!-- Aqui inicia el carrusel -->
{!! Html::script('js/jquery-1.11.3.min.js') !!}
{!! Html::script('js/jssor.slider-21.1.5.mini.js') !!}
<!-- use jssor.slider-21.1.5.debug.js instead for debug -->
{!! Html::script('js/carousel-1.js') !!}
<style>
/* jssor slider bullet navigator skin 03 css */
/*
.jssorb03 div           (normal)
.jssorb03 div:hover     (normal mouseover)
.jssorb03 .av           (active)
.jssorb03 .av:hover     (active mouseover)
.jssorb03 .dn           (mousedown)
*/
.jssorb03 {
position: absolute;
}
.jssorb03 div, .jssorb03 div:hover, .jssorb03 .av {
position: absolute;
/* size of bullet elment */
width: 21px;
height: 21px;
text-align: center;
line-height: 21px;
color: white;
font-size: 12px;
background: url('img/b03.png') no-repeat;
overflow: hidden;
cursor: pointer;
}
.jssorb03 div { background-position: -5px -4px; }
.jssorb03 div:hover, .jssorb03 .av:hover { background-position: -35px -4px; }
.jssorb03 .av { background-position: -65px -4px; }
.jssorb03 .dn, .jssorb03 .dn:hover { background-position: -95px -4px; }
/* jssor slider arrow navigator skin 03 css */
/*
.jssora03l                  (normal)
.jssora03r                  (normal)
.jssora03l:hover            (normal mouseover)
.jssora03r:hover            (normal mouseover)
.jssora03l.jssora03ldn      (mousedown)
.jssora03r.jssora03rdn      (mousedown)
*/
.jssora03l, .jssora03r {
display: block;
position: absolute;
/* size of arrow element */
width: 55px;
height: 55px;
cursor: pointer;
background: url('img/a03.png') no-repeat;
overflow: hidden;
}
.jssora03l { background-position: -3px -33px; }
.jssora03r { background-position: -63px -33px; }
.jssora03l:hover { background-position: -123px -33px; }
.jssora03r:hover { background-position: -183px -33px; }
.jssora03l.jssora03ldn { background-position: -243px -33px; }
.jssora03r.jssora03rdn { background-position: -303px -33px; }
</style>
<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden; visibility: hidden;">
  <!-- Loading Screen -->
  <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
  </div>
  <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden;">
    <div style="display: none;">
      <img data-u="image" src="img/005.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/006.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/011.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/013.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/014.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/019.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/020.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/021.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/022.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/024.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/025.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/027.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/029.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/030.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/031.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/030.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/034.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/038.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/039.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/043.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/044.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/047.jpg" />
    </div>
    <div style="display: none;">
      <img data-u="image" src="img/050.jpg" />
    </div>
    <a data-u="add" href="http://www.jssor.com/demos/carousel-slider.slider" style="display:none">Carousel</a>
    
  </div>
  <!-- Bullet Navigator -->
  <div data-u="navigator" class="jssorb03" style="bottom:10px;right:10px;">
    <!-- bullet navigator item prototype -->
    <div data-u="prototype" style="width:21px;height:21px;">
      <div data-u="numbertemplate"></div>
    </div>
  </div>
  <!-- Arrow Navigator -->
  <span data-u="arrowleft" class="jssora03l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
  <span data-u="arrowright" class="jssora03r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"></span>
</div>
<!-- Aqui Termina -->
</div>
</div <div class="wrapper3">
<div class="triangle-down3">
<div class="fondo3">
<div></div>
</div>
</div>
</div>
</div>
</div>
<!--OUR LAST HISTORY-->

<div class="container-fluid text-center">
<div class="content4">
<h3 class="h3-triangulo">Our Last History</h3>
<p>Quis ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
<div class="cuadrado-2 center-block"></div>
@yield('content')
</div> 
<div class="triangle-down4">
<div class="fondo4">
<div></div>
</div>
</div>
</div>

</div>
<div class="container-fluid circles">
<div class="imagen-1">
<img src="img/footer.jpg" class="imagen-1">
</div>
</div>
<!-- Footer -->
<footer class="container-fluid bg-4 text-center">
<div class="row row-footer">
<div class="col-sm-4 col-4-custom">
<h3><strong>About us</strong></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, consectetur adipisicing elit, consectetur adipisicing elit, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
</div>
<div class="col-sm-4">
<h3><strong>Quick links</strong></h3>
<ul class="li-footer">
<li><img src="img/flecha.png" class="icon-footer" alt="Image">&nbsp;  ALERTS AND STUFF</li>
<li><img src="img/flecha.png" class="icon-footer" alt="Image">&nbsp;  CUSTOM SHORTCOODES</li>
<li><img src="img/flecha.png" class="icon-footer" alt="Image">&nbsp;  PRICING PLANS</li>
<li><img src="img/flecha.png" class="icon-footer" alt="Image">&nbsp;  TOGGLE ELEMENTS</li>
<li><img src="img/flecha.png" class="icon-footer" alt="Image">&nbsp;  TYPOGRAPHY</li>
</ul>
</div>
<div class="col-sm-4">
<h3><strong>Get in touch</strong></h3>
<ul class="li-footer2">
<li><img src="img/casa.png" class="icon-footer2" alt="Image">&nbsp;66 Suth Street</li>
<li class="li2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Window 6 Wonderwall</li>
<li><img src="img/telefono.png" class="icon-footer3" alt="Image"> +440 123456789 - Office</li>
<li class="li3">+440 123456789 - Fax</li>
<li><img src="img/mail.png" class="icon-footer2" alt="Image">&nbsp; Contact@Mail.com</li>
</ul>
</div>
</div>
</footer>
</body>
</html>