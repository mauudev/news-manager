$(document).ready(function(){
	loadData();
});

function loadData(){
	var dataTable1 = $("#dataTable1");
	var currentURL = window.location.href//nueva ruta
	currentURL = currentURL.replace("local/news","local/newsList");
	dataTable1.empty();
	$.get(currentURL,function(res){
		$(res).each(function(key,value){
			var path = value.picturepath;
			var length = 50;
		    var truncateTitle = value.title;
		    var truncateText = value.text;
	        var truncatedTitle = truncateTitle.substring(0,length)+"...";
	        var truncatedText = truncateText.substring(0,length)+"...";
			if(path == ''){
				 var path2 = "<td><img src='images/no-image.jpg' alt='' style='width:100px'/></td>";
	        	 var buttons = "<td><button value='"+value.id+"'onClick='Show(this);'type='button' class='btn btn-success btn-circle glyphicon glyphicon-edit' data-toggle='modal' data-target='#myModal' data-tooltip='Edit news'></button> <button value='"+value.id+"'onClick='Delete(this);'type='button' class='btn btn-danger btn-circle glyphicon glyphicon-trash' data-tooltip='Delete news'></button></td>";
				 dataTable1.append("<tr><td>"+value.id+"</td><td>"+truncatedTitle+"</td><td>"+truncatedText+"</td>"+path2+buttons+"</tr>");
        	}else{
	        	 var path2 = "<td><img src='images/"+path+"' alt='' style='width:100px'/></td>";
	        	 var buttons = "<td><button value='"+value.id+"'onClick='Show(this);'type='button' class='btn btn-success btn-circle glyphicon glyphicon-edit' data-toggle='modal' data-target='#myModal' data-tooltip='Edit news'></button> <button value='"+value.id+"'onClick='Delete(this);'type='button' class='btn btn-danger btn-circle glyphicon glyphicon-trash' data-tooltip='Delete news'></button></td>";
				 dataTable1.append("<tr><td>"+value.id+"</td><td>"+truncatedTitle+"</td><td>"+truncatedText+"</td>"+path2+buttons+"</tr>");
			}
		});
	});
}

function Show(btn){
	//alert(btn.value);
	var headTittle = $("#headTittle");
	var title1 = $("#newsTitle");
	var text1 = $("#newsText");
	var currentURL = window.location.href+"/"+btn.value+"/edit";//nueva ruta
	$.get(currentURL,function(res){
		headTittle.empty();
		headTittle.append(res.title);
		$("#newsTitle").val(res.title);
		$("#newsText").val(res.text);
		$("#id").val(res.id);
	});
}

function Delete(btn){
	var currentURL = window.location.href+"/"+btn.value+"";
	alert(currentURL);
	var token = $("#token").val();
	$.ajax({
		url:currentURL,
		headers:{'X-CSRF-TOKEN':token},
		type:'DELETE',
		dataType:'json',
		success:function(){
			loadData();
			$("#delete-success-ajax").fadeIn();
		}
	});
}
$("#update").click(function(){
	var value = $("#id").val();
	alert(value);
	var titleData = $("#newsTitle").val();
	var textData = $("#newsText").val();
	var currentURL = window.location.href+"/"+value+"";
	alert(currentURL);
	var token = $("#token").val();
	$.ajax({
		url:currentURL,
		headers:{'X-CSRF-TOKEN':token},
		type:'PUT',
		dataType:'json',
		data:{title:titleData,text:textData},
		success:function(){
			loadData();
			$("#myModal").modal('toggle');
			$("#update-success-ajax").fadeIn();
		}
	});
});