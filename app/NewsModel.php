<?php

namespace NewsManager;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Storage;
use File;
use DB;

class NewsModel extends Model
{
    protected $table = 'news';
    protected $fillable = ['title','text','picturepath','administrators_id'];

    public function setPicturePathAttribute($picturepath){//hace referencia al campo path
	    if(!empty($picturepath)){
	    $name = Carbon::now()->second.$picturepath->getClientOriginalName();
	    $this->attributes['picturepath'] = $name;
	    Storage::disk('local')->put($name, File::get($picturepath));
	    }
    }
    public static function getAll(){
    	return DB::table('news')
    			   ->select('id','title','text','picturepath','created_at','updated_at','administrators_id')
    			   ->paginate(5);
    }
    public static function getRandom($cant){
        return DB::table('news')
                   ->select('id','title','text','picturepath','created_at','updated_at','administrators_id')
                   ->first();
    }
}
