<?php

namespace NewsManager\Http\Controllers;

use Illuminate\Http\Request;
use NewsManager\NewsModel;
use NewsManager\Http\Requests;

class AjaxReadController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function ajaxList(){
        $news = NewsModel::all();
        return response()->json($news->toArray());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = NewsModel::getAll();
        return view('admin-ajax.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-ajax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NewsModel::create($request->all());
        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = NewsModel::find($id);
        return response()->json($new->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new = NewsModel::find($id);
        $new->fill($request->all());
        $new->save();
        return response()->json(['mensaje'=>'updated !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = NewsModel::find($id);
        $new->delete();
        return response()->json(['message'=>'deleted !']);
    }
}
