<?php

namespace NewsManager\Http\Controllers;

use Illuminate\Http\Request;
use NewsManager\NewsModel;
use NewsManager\Http\Requests;
use Session;
class NewsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $news = NewsModel::getAll();
        return view('admin.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        NewsModel::create($request->all());
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = NewsModel::find($id);
        return view('admin.show',['new'=>$new]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = NewsModel::find($id);
        return view('admin.edit',['new'=>$new]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $new = NewsModel::find($id);
            $new->title = $request->title;
            $new->text = $request->text;
            if($request->picturepath == null){
                $new->save();
                Session::flash('update-success',"Data updated correctly !");
                return redirect()->route('admin.index');
            }
            else $new->picturepath = $request->picturepath;
            $new->save();
            Session::flash('update-success',"Data updated correctly !");
            return redirect()->route('admin.index');
        }catch(Exception $e){
            Session::flash('error-message',"Something's failed during updating: ".$e);
            return redirect()->route('admin.index');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = NewsModel::find($id);
        $new->delete();
        Session::flash('delete-success','Record removed correctly!!');
        return redirect()->route('admin.index');
    }
}
