<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::resource('admin','NewsController');
Route::get('admin/{admin}/destroy',[
  'uses'=>'NewsController@destroy',
  'as' => 'admin.destroy'
]);
Route::resource('login','LoginController');
Route::get('logout','LoginController@logout');
Route::resource('register','AdministratorController');
Route::resource('news','AjaxReadController');
Route::get('news/{news}/destroy',[
  'uses'=>'AjaxReadController@destroy',
  'as' => 'news.destroy'
]);
Route::get('newsList','AjaxReadController@ajaxList');

Route::get('push','HomeController@ajaxList');