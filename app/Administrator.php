<?php

namespace NewsManager;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class Administrator extends Authenticatable
{
    protected $table = 'administrators';
    protected $fillable = [
        'name','last_name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getRows_count(){
        return DB::table("administrators")->count();         
    }
}
